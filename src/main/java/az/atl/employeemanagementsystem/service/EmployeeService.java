package az.atl.employeemanagementsystem.service;

import az.atl.employeemanagementsystem.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    List<Employee> getAllEmployees();
    void save(Employee employee);

    Employee getById(Long id);

    void deleteById(Long id);
}
