package az.atl.employeemanagementsystem.service.impl;

import az.atl.employeemanagementsystem.model.Employee;
import az.atl.employeemanagementsystem.repository.EmployeeRepository;
import az.atl.employeemanagementsystem.service.EmployeeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public record EmployeeServiceImpl(EmployeeRepository employeeRepository) implements EmployeeService {
    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public void save(Employee employee) {
        if (Objects.nonNull(employee)) {
            employeeRepository.save(employee);
        }
    }

    @Override
    public Employee getById(Long id) {

        Employee employee = null;
        if (Objects.nonNull(id)) {
            Optional<Employee> optionalEmployee = employeeRepository.findById(id);
            if (optionalEmployee.isPresent()) {
                employee = optionalEmployee.get();
            } else {
                throw new RuntimeException("Employee not found with this id: " + id);
            }
        }
        return employee;
    }

    @Override
    public void deleteById(Long id) {
        if (Objects.nonNull(id)){
            employeeRepository.deleteById(id);
        }
    }
}
